%global _empty_manifest_terminate_build 0
Name:           python-txaio
Version:        22.2.1
Release:        1
Summary:        Compatibility API between asyncio/Twisted/Trollius
License:        MIT
URL:            https://github.com/crossbario/txaio
Source0:        https://files.pythonhosted.org/packages/6d/4b/28313388dfb2bdedb71b35b900459c56ba08ccb7ad2885487df037808c06/txaio-22.2.1.tar.gz
BuildArch:      noarch

Requires:       python3-zope-interface
Requires:       python3-twisted
Requires:       python3-wheel
Requires:       python3-pytest
Requires:       python3-pytest-cov
Requires:       python3-pep8
Requires:       python3-sphinx
Requires:       python3-enchant
Requires:       python3-sphinxcontrib-spelling
Requires:       python3-sphinx_rtd_theme
Requires:       python3-tox
Requires:       python3-mock
Requires:       python3-twine

%description
| |Version| |Build| |Deploy| |Coverage| |Docs|--**txaio*is a helper library for
writing code that runs unmodified on both Twisted < and asyncio < / Trollius <
is like six < but for wrapping over differences between Twisted and asyncio so
one can write code that runs unmodified on both (aka *source code
compatibility*). In other words: your *userscan choose if they want asyncio
**or*Twisted as a dependency.Note that, with this approach, user code **runs
under the native event loop of either Twisted or asyncio**.


%package -n python3-txaio
Summary:        Compatibility API between asyncio/Twisted/Trollius
Provides:       python-txaio
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
%description -n python3-txaio
| |Version| |Build| |Deploy| |Coverage| |Docs|--**txaio*is a helper library for
writing code that runs unmodified on both Twisted < and asyncio < / Trollius <
is like six < but for wrapping over differences between Twisted and asyncio so
one can write code that runs unmodified on both (aka *source code
compatibility*). In other words: your *userscan choose if they want asyncio
**or*Twisted as a dependency.Note that, with this approach, user code **runs
under the native event loop of either Twisted or asyncio**.


%package help
Summary:        Development documents and examples for txaio
Provides:       python3-txaio-doc
%description help
| |Version| |Build| |Deploy| |Coverage| |Docs|--**txaio*is a helper library for
writing code that runs unmodified on both Twisted < and asyncio < / Trollius <
is like six < but for wrapping over differences between Twisted and asyncio so
one can write code that runs unmodified on both (aka *source code
compatibility*). In other words: your *userscan choose if they want asyncio
**or*Twisted as a dependency.Note that, with this approach, user code **runs
under the native event loop of either Twisted or asyncio**.


%prep
%autosetup -n txaio-22.2.1

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
touch filelist.lst
if [ -d usr/lib64 ]; then
        find usr/lib64 -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
        find usr/bin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
        find usr/sbin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
        find usr/share/man -type f -printf "\"/%h/%f.gz\"\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-txaio -f filelist.lst
%{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Oct 29 2024 liutao1 <liutao1@kylinos.cn> - 22.2.1-1
- Init package python3-txaio of version 22.2.1

* Mon Apr 17 2023 OpenStack_SIG <openstack@openeuler.org> - 21.2.1-1
- Init package python3-txaio of version 21.2.1
